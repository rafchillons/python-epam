#2.1
def switch_quotes(s):
    result = ''

    for letter in s:
        if letter == '"':
            result += '\''
        elif letter == '\'':
            result += '"'
        else:
            result += letter
    
    return result


#2.2
def is_palindrome(s):
    for i in range((len(s) // 2)):
        if s[i] != s[-(i + 1)]:
            return False
    return True


#2.3
def split(s):
    result = []
    word = ''
    
    for i in range(len(s)):
        if s[i] != ' ':
            word = word + s[i]
        else: 
            result.append(word)
            word = ''
            i = i + 1
        if i == (len(s) - 1):
            result.append(word)
   
    return result


#2.4
def split_by_index(s: str, indexes: [int]) -> [str]:
    result = []

    for i in range(len(indexes)):
        if i == 0:
            result.append(s[:indexes[i]])
        elif indexes[i] == indexes[-1]:
            result.append(s[indexes[i - 1]: indexes[i]])
            result.append(s[indexes[i]:]) 
        else:
            result.append(s[indexes[i - 1]: indexes[i]])
   
    return result


#2.5
def get_digits(num: int) -> (int):
    return tuple([int(digit) for digit in str(num)]) 


#2.6
def get_longest_word(s: str) -> str:
    words = s.split()
    temp = 0
    longest = 0
    result = ''

    for word in words: 
        for letter in word:
            temp = temp + 1
        if temp > longest:
            longest = temp
            result = word
    temp = 0 

    return result

#2.7
def foo(test: [int]) -> [int]:
    temp = 1
    result = []

    for i in test:
        temp = 1
        for j in range(len(test)):
            temp = temp * test[j]
        result.append(int(temp / i))

    return result


#2.8
def get_pairs(lst: []) -> []:
    temp = []
    result = []
    n = len(lst)

    if n == 1: 
        return None

    for pairOne in range(n):
        if pairOne == (n - 1):
            break
        else: 
            temp.append(lst[pairOne])
            temp.append(lst[pairOne + 1])
            result.append(tuple(temp))
            temp = []

    return result